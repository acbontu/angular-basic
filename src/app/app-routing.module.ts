import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasicFormComponent } from './Binding/basic-form/basic-form.component';
import { NoPageComponent } from './no-page/no-page.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { RoutingComponent } from './routing/routing.component';

const routes: Routes = [
  {
    component: BasicFormComponent, path: 'basic-form'
  },
  {
    component: RoutingComponent, path: 'routing'
  },
  {
    component: ReactiveFormComponent, path: 'reactive-form'
  },
  {
    component: NoPageComponent, path: '**'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
