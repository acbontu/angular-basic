import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  constructor() { }
  @Input() item = '';
  ngOnInit(): void {
  }
  getValue!: string;
  i = 0;
  getData(name: string) {
    this.getValue = name;
  }

  count() {
    this.i++;
  }

}
