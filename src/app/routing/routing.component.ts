import { Component, OnInit } from '@angular/core';
import { UsersService } from '../Services/users.service';
import { GetApiDataService } from '../Services/get-api-data.service';
@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.css']
})
export class RoutingComponent implements OnInit {
  fetchedData: any;
  link: any;
  constructor(private userdata: UsersService, private apiData: GetApiDataService) {
    this.apiData.getData().subscribe((data) => {
      this.fetchedData = data;
    });
    this.link = this.apiData.url;
  }

  ngOnInit(): void {
  }

  users = this.userdata.users();
  delete(id: number) {
    this.users.splice(id, 1);
  }

}
