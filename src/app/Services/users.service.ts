import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  [x: string]: any;

  constructor() { }

  users() {
    return [
      {
        name: 'Ontu Chakrabarti',
        phone: [{ gp: '01733978467', bl: '01933978467', robi: '01833978467' }],
        location: 'Dhaka'
      },
      {
        name: 'Aumit Chakrabarti',
        phone: [{ gp: '01733978467', bl: '01933978467', robi: '01833978467' }],
        location: 'Dhaka'
      },
      {
        name: 'Pijush Chakrabarti',
        phone: [{ gp: '01733978467', bl: '01933978467', robi: '01833978467' }],
        location: 'Dhaka'
      }
    ];

  }
}
