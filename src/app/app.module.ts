import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventComponent } from './Event/event/event.component';
import { PropertyBindingComponent } from './Binding/property-binding/property-binding.component';
import { LoopComponent } from './Binding/loop/loop.component';
import { HeaderComponent } from './header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BasicFormComponent } from './Binding/basic-form/basic-form.component';
import { FormsModule } from '@angular/forms';
import { PipeComponent } from './pipe/pipe.component';
import { CustomPipe } from './custom.pipe';
import { RoutingComponent } from './routing/routing.component';
import { NoPageComponent } from './no-page/no-page.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    EventComponent,
    PropertyBindingComponent,
    LoopComponent,
    HeaderComponent,
    BasicFormComponent,
    PipeComponent,
    CustomPipe,
    RoutingComponent,
    NoPageComponent,
    ReactiveFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
