import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'practice-step-by-step';
  data = 'nothing';
  parentData(item: string) {
    console.log(item);
    this.data = item;
  }
}
