import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  templateUrl: './property-binding.component.html',
  styleUrls: ['./property-binding.component.css']
})
export class PropertyBindingComponent implements OnInit {
  value = 'click me';
  class = 'btn btn-info btn-lg';
  condition = true;
  bgcolor = '';
  constructor() { }

  ngOnInit(): void {
  }
  valueFunction(name: string) {
    if (name === 'primary') {
      this.class = 'btn btn-primary btn-lg';
    }
    else if (name === 'success') {
      this.class = 'btn btn-success btn-lg';
    }
    else if (name === 'danger') {
      this.class = 'btn btn-danger btn-lg';
    }
  }

  changeBackground() {
    if (this.bgcolor === '') {
      this.bgcolor = 'yellow';
    } else {
      this.bgcolor = '';
    }

  }
}
