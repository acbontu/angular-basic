import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basic-form',
  templateUrl: './basic-form.component.html',
  styleUrls: ['./basic-form.component.css']
})
export class BasicFormComponent implements OnInit {
  @Output() dataPassingEvent = new EventEmitter<string>();
  constructor() { }
  @Input() item = '';
  bindingData: any;
  ngOnInit(): void {
  }

  getFormData(data: NgForm) {
    console.log(data);
  }

}
